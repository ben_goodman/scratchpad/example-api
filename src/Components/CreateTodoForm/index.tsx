import React from 'react'

import Input from '@mui/joy/Input'
import Textarea from '@mui/joy/Textarea'
import Button from '@mui/joy/Button'

interface CreateTodoFormProps {
    onSubmit: (name: string, description: string) => Promise<void>,
}

const CreateTodoForm = (props: CreateTodoFormProps) => {
    const [name, setName] = React.useState<string>('')
    const [description, setDescription] = React.useState<string>('')
    const [isLoading, setIsLoading] = React.useState<boolean>(false)

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        setIsLoading(true)
        e.preventDefault()

        await props.onSubmit(name, description)

        setIsLoading(false)
    }

    return (
        <form onSubmit={handleSubmit}>
            <Input
                placeholder="Name"
                value={name}
                onChange={e => setName(e.target.value)}
                required
            />

            <Textarea
                placeholder="Description"
                value={description}
                onChange={e => setDescription(e.target.value)}
                required
            />

            <Button
                type="submit"
                loading={isLoading}
            >
                Add
            </Button>
        </form>
    )
}

export default CreateTodoForm

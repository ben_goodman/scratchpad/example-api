import React from 'react'

import Input from '@mui/joy/Input'
import Button from '@mui/joy/Button'

import useAuthentication from 'src/Hooks/useAuthentication'
import { Typography } from '@mui/joy'

export interface LoginFormProps {
    onSuccess: () => void
}

const LoginForm = (props: LoginFormProps) => {
    const [isLoading, setIsLoading] = React.useState<boolean>(false)
    const [error, setError] = React.useState<string | undefined>(undefined)
    const [username, setUsername] = React.useState<string>('')
    const [password, setPassword] = React.useState<string>('')

    const { login } = useAuthentication()

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        setIsLoading(true)
        e.preventDefault()

        login(username, password)
            .then(() => {
                props.onSuccess()
            })
            .catch((err) => {
                setError(err.message)
            })
            .finally(() => {
                setIsLoading(false)
            })
    }

    // unset error after 4 seconds
    React.useEffect(() => {
        if (error) {
            const timeout = setTimeout(() => {
                setError(undefined)
            }, 4000)

            return () => {
                clearTimeout(timeout)
            }
        }
    }, [error])

    return (
        <>
            <form onSubmit={handleSubmit}>
                <Input
                    placeholder="Username"
                    value={username}
                    onChange={e => setUsername(e.target.value)}
                    required
                />

                <Input
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    type='password'
                    required
                />

                <Button
                    type="submit"
                    loading={isLoading}
                >
                    Login
                </Button>

                <Typography>
                    {error}
                </Typography>
            </form>
        </>
    )
}

export default LoginForm

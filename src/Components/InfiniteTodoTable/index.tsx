import React from 'react'

import Table from '@mui/joy/Table'
import Dropdown from '@mui/joy/Dropdown'
import MenuButton from '@mui/joy/MenuButton/MenuButton'
import MenuItem from '@mui/joy/MenuItem/MenuItem'
import Menu from '@mui/joy/Menu/Menu'

import { TodoItem, TodoKey } from 'src/Hooks/useTodo'

export interface TodoTableProps {
    items: TodoItem[]
    onToggleStatusClick: (key: TodoKey) => void
    onDeleteClick: (key: TodoKey) => void
    onLoadMore: () => void
    showActions: boolean
}

const InfiniteTodoTable = (props: TodoTableProps) => {
    const [lastElementObserver, setLastElementObserver] = React.useState<IntersectionObserver | null>(null)

    // if there is a change to the loaded items,
    // add an intersection observer to thr last item.
    React.useEffect(() => {
        // add intersection observer to last element
        if (lastElementObserver) {
            lastElementObserver.disconnect()
        }

        const observer = new IntersectionObserver(
            entries => {
                if (entries[0].isIntersecting) {
                    props.onLoadMore()
                }
            }
        )
        const lastItem = document.querySelector('#scroll-container > *:last-of-type')

        if (lastItem) {
            observer.observe(lastItem)
            setLastElementObserver(observer)
        }
    }, [props.items])

    return (
        <Table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Status</th>
                    {props.showActions && <th>Actions</th>}
                </tr>
            </thead>
            <tbody id='scroll-container'>
                {props.items.map((item) => {
                    const key: TodoKey = {name: item.name, dateCreated: item.dateCreated}
                    return <tr key={`${item.isCompleted}#${item.dateCreated}`}>
                        <td>{item.name}</td>
                        <td>{item.description}</td>
                        <td>{item.isCompleted ? 'Completed' : 'Pending'}</td>
                        {props.showActions &&
                            <Dropdown>
                                <MenuButton>...</MenuButton>
                                <Menu>
                                    <MenuItem onClick={() => props.onToggleStatusClick(key)}>Toggle</MenuItem>
                                    <MenuItem onClick={() => props.onDeleteClick(key)}>Delete</MenuItem>
                                </Menu>
                            </Dropdown>
                        }
                    </tr>
                })}
            </tbody>
        </Table>

    )
}

export default InfiniteTodoTable

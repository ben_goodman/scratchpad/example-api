import React from 'react'

import Modal from '@mui/joy/Modal/Modal'
import ModalDialog from '@mui/joy/ModalDialog/ModalDialog'
import ModalClose from '@mui/joy/ModalClose/ModalClose'
import ButtonGroup from '@mui/joy/ButtonGroup/ButtonGroup'
import Button from '@mui/joy/Button/Button'

import LoginForm from './Components/LoginForm'
import InfiniteTodoTable from './Components/InfiniteTodoTable'
import TodoForm from './Components/CreateTodoForm'

import useTodo, { TodoItem, TodoKey } from './Hooks/useTodo'
import useAuthentication from './Hooks/useAuthentication'

const App = () => {
    const todo = useTodo()
    const auth = useAuthentication()
    const [items, setItems] = React.useState<TodoItem[]>([])
    const [lastEvaluatedKey, setLastEvaluatedKey] = React.useState<TodoKey | undefined>(undefined)

    const [isLoginModalOpen, setIsLoginModalOpen] = React.useState(false)
    const [isNewTodoModalOpen, setIsNewTodoModalOpen] = React.useState(false)

    const loadData = async () => {
        todo.listTodo({limit: 3, from: lastEvaluatedKey}).then((resp) => {
            setItems([...items, ...resp.Items])
            setLastEvaluatedKey(resp.LastEvaluatedKey)
        })
    }

    const handleScrollEnd = async () => {
        if (!lastEvaluatedKey) return
        await loadData()
    }

    React.useEffect(() => {
        loadData()
    }, [])

    const handleNewTodo = async (name: string, description: string) => {
        console.log('Todo created')
        const newItem = await todo.createTodo(name, description)
        setItems([newItem, ...items])
        setIsNewTodoModalOpen(false)
    }

    const handleToggleComplete = async (key: TodoKey) => {
        const resp = await todo.toggleTodo(key)
        const newState = resp.Attributes.isCompleted!
        setItems(items.map((item) => {
            if (item.name === key.name && item.dateCreated === key.dateCreated) {
                return {...item, isCompleted: newState}
            }
            return item
        }))
    }

    const handleDelete = async (key: TodoKey) => {
        await todo.deleteTodo(key)
        setItems(items.filter((item) => item.name !== key.name && item.dateCreated !== key.dateCreated))
    }

    return (
        <>
            <Modal
                open={isLoginModalOpen}
                onClose={() => setIsLoginModalOpen(false)}
            >
                <ModalDialog>
                    <ModalClose />
                    <LoginForm
                        onSuccess={() => {
                            setIsLoginModalOpen(false)
                            loadData()
                        }}
                    />
                </ModalDialog>
            </Modal>

            <Modal
                open={isNewTodoModalOpen}
                onClose={() => setIsNewTodoModalOpen(false)}
            >
                <ModalDialog>
                    <ModalClose />
                    <TodoForm onSubmit={handleNewTodo} />
                </ModalDialog>
            </Modal>

            <header>
                <ButtonGroup>
                    {auth.isAuthenticated()
                        ? <Button onClick={() => auth.logout()}>Logout</Button>
                        : <Button onClick={() => setIsLoginModalOpen(true)}>Login</Button>
                    }
                    {auth.isAuthenticated() &&
                        <Button onClick={() => setIsNewTodoModalOpen(true)}>New Todo</Button>
                    }
                </ButtonGroup>
            </header>

            <InfiniteTodoTable
                items={items}
                onToggleStatusClick={handleToggleComplete}
                onDeleteClick={handleDelete}
                showActions={auth.isAuthenticated()}
                onLoadMore={handleScrollEnd}
            />
        </>
    )
}

export default App

import useAuthentication from './useAuthentication'

export interface ServerCollectionResponse<T> {
    Items: T[]
    LastEvaluatedKey: TodoKey
    Count: number
    ScannedCount: number
}



export interface TodoItem {
    name: string
    description: string
    isCompleted: boolean
    dateCreated: number
}


export interface ModifyItemResponse<T> {
    Attributes: Partial<T>
}

export type TodoKey = {
    name: string
    dateCreated: number
}

export interface ScanOptions {
    limit?: number
    from?: TodoKey,
}

const useTodo = () => {
    const { getToken } = useAuthentication()

    return {
        queryTodo: async (query: string): Promise<ServerCollectionResponse<TodoItem>> => {
            const url = new URL(`./api/todo/query/${query}`, window.location.href)

            const resp = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            return resp.json()
        },

        listTodo: async ({from, limit}: ScanOptions = {}): Promise<ServerCollectionResponse<TodoItem>> => {
            const url = new URL('./api/todo', window.location.href)
            from && url.searchParams.append('from', JSON.stringify(from))
            limit && url.searchParams.append('limit', limit.toString())

            const resp = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            return resp.json()
        },

        createTodo: async (name: string, description: string): Promise<TodoItem> => {
            const url = new URL('./api/todo', window.location.href)
            const formData = new FormData()
            formData.append('name', name)
            formData.append('description', description)

            const resp = await fetch(url, {
                method: 'POST',
                body: formData,
                headers: {
                    'authorization': getToken() || '',
                }
            })
            return resp.json()
        },

        toggleTodo: async (key: TodoKey): Promise<ModifyItemResponse<TodoItem>> => {
            const url = new URL('./api/todo', window.location.href)
            const formData = new FormData()
            formData.append('name', key.name.toString())
            formData.append('dateCreated', key.dateCreated.toString())

            const resp = await fetch(url, {
                method: 'PATCH',
                body: formData,
                headers: {
                    'authorization': getToken() || '',
                }
            })
            return resp.json()
        },

        deleteTodo: async (key: TodoKey): Promise<ModifyItemResponse<TodoItem>> => {
            const url = new URL('./api/todo', window.location.href)
            const formData = new FormData()
            formData.append('name', key.name.toString())
            formData.append('dateCreated', key.dateCreated.toString())

            const resp = await fetch(url, {
                method: 'DELETE',
                body: formData,
                headers: {
                    'authorization': getToken() || '',
                }
            })
            return resp.json()
        },
    }
}

export default useTodo

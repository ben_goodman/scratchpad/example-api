# Example API

A boilerplate TODO API with React frontend.

# Setup
1. Create a new user in the Cognito user pool
1. Validate the new user
    ```bash
    aws cognito-idp admin-set-user-password \
    --user-pool-id 'us-east-1_xyz' \
    --username 'example-user' \
    --password 'password1234' --permanent
    ```

resource "aws_dynamodb_table" "default" {
    name             = "${var.dynamo_table_name}-${random_id.cd_function_suffix.hex}-${terraform.workspace}"
    hash_key         = "name"
    range_key        = "dateCreated"
    billing_mode     = "PAY_PER_REQUEST"

    attribute {
        name = "name"
        type = "S"
    }

    attribute {
        name = "dateCreated"
        type = "N"
    }
}

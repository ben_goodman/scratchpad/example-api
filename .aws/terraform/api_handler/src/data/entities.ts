import { Entity } from 'dynamodb-toolbox/entity'
import TodoSchema from './schema'
import TodoTable from './table'

const TodoEntity = new Entity({
    name: 'TODO',
    table: TodoTable,
    schema: TodoSchema,
})

export default TodoEntity

import { schema } from 'dynamodb-toolbox/schema'
import { string } from 'dynamodb-toolbox/attributes/string'
import { number } from 'dynamodb-toolbox/attributes/number'
import { boolean } from 'dynamodb-toolbox'

const validateName = (value: string) => (
    value.length > 3 && value.length < 120
        ? true
        : 'Name must be between 3 and 60 characters long.'
)

const validateDescription = (value: string) => (
    value.length > 3 && value.length < 200
        ? true
        : 'Description must be between 3 and 200 characters long.'
)

const todoSchema = schema({
    name: string().key().validate(validateName),
    dateCreated: number().key().default(() => Date.now()),
    description: string().validate(validateDescription),
    isCompleted: boolean().default(false),
})

export default todoSchema

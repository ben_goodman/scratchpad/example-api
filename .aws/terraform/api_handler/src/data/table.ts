import { Table } from 'dynamodb-toolbox/table'
import { DynamoDBClient } from '@aws-sdk/client-dynamodb'
import { DynamoDBDocumentClient } from '@aws-sdk/lib-dynamodb'

const dynamoDBClient = new DynamoDBClient()

const documentClient = DynamoDBDocumentClient.from(dynamoDBClient)

const name = process.env.TABLE_NAME!

const table = new Table({
    name,
    documentClient,
    partitionKey: {
        name: 'name',
        type: 'string',
    },
    sortKey: {
        name: 'dateCreated',
        type: 'number',
    },
})

export default table

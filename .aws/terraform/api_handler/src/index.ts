import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'

import toggleItem from './events/toggle'
import deleteItem from './events/delete'
import createItem from './events/create'
import listItems from './events/list'
import queryItem from './events/query'

export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    console.log('event', event)
    const  { resource, httpMethod } = event

    const resourceId = `${httpMethod} ${resource}`

    switch (resourceId) {
    case 'GET /api/todo':
        return listItems(event)
    case 'GET /api/todo/{query}':
        return queryItem(event)
    case 'POST /api/todo':
        return createItem(event)
    case 'PATCH /api/todo':
        return toggleItem(event)
    case 'DELETE /api/todo':
        return deleteItem(event)
    case 'GET /api/health':
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ message: 'OK' }),
        }
    default:
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ message: 'Not Found' }),
        }
    }
}

export default class HTTPError {
    public static fromError = (error: Error): HTTPError => {
        console.error(error)
        return new HTTPError(
            error.name,
            error.message,
            500
        )
    }

    constructor(
        public name: string,
        public message: string,
        public code: number
    ) {}
}

export const BadRequest = (msg: string = 'Invalid Input') =>
    new HTTPError('BadRequest', msg, 400)

export const ok = (condition: unknown, err: HTTPError) => {
    if (!condition) {
        throw err
    }
}

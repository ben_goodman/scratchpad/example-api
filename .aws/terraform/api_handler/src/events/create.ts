import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'
import { PutItemCommand } from 'dynamodb-toolbox'
import { parse } from 'lambda-multipart-parser'
import TodoEntity from '../data/entities'
import HTTPError, { BadRequest, ok } from '../HTTPError'


const createItem = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    try {
        const {name, description} = await parse(event)
        ok(name && description, BadRequest('Missing `name` or `description`'))

        const item = {
            name,
            description,
            isCompleted: false,
            dateCreated: Date.now(),
        }

        await TodoEntity
            .build(PutItemCommand)
            .item(item)
            .options({
                condition: {
                    attr: 'name',
                    exists: false,
                },
            })
            .send()

        return {
            statusCode: 201,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(item),
        }
    } catch (err) {
        const { message } = err as HTTPError
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ message }),
        }
    }
}

export default createItem

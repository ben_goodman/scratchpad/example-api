import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'
import { ScanCommand } from 'dynamodb-toolbox'
import TodoTable from '../data/table'

const listItems = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    const { from, limit } = event.queryStringParameters || {}

    console.log('page data: ', limit, from)

    const resp = await TodoTable
        .build(ScanCommand)
        .options({
            limit: limit ? parseInt(limit) : 10,
            exclusiveStartKey: from ? JSON.parse(from) : undefined,
        })
        .send()

    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(resp),
    }
}

export default listItems

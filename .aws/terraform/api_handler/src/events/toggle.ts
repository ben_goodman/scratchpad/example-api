import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'
import { parse } from 'lambda-multipart-parser'
import { GetItemCommand } from 'dynamodb-toolbox/entity/actions/get'
import { UpdateItemCommand } from 'dynamodb-toolbox/entity/actions/update'
import TodoEntity from '../data/entities'
import { BadRequest, ok } from '../HTTPError'

const toggleItem = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    try {
        const {name, dateCreated} = await parse(event)
        ok(name && dateCreated, BadRequest('Missing `name` or `dateCreated`'))

        const key = {
            name,
            dateCreated: parseInt(dateCreated),
        }

        // get the current value of isCompleted
        const { Item } = await TodoEntity
            .build(GetItemCommand)
            .key(key)
            .send()

        ok(Item, BadRequest('Item not found'))

        const resp = await TodoEntity
            .build(UpdateItemCommand)
            .item({
                ...key,
                isCompleted: !Item!.isCompleted,
            })
            .options({
                returnValues: 'UPDATED_NEW',
            })
            .send()

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(resp),
        }
    } catch (err) {
        const { message } = err as Error
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ message }),
        }
    }
}

export default toggleItem

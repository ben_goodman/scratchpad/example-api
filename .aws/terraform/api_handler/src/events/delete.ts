import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'
import { DeleteItemCommand } from 'dynamodb-toolbox'
import { parse } from 'lambda-multipart-parser'
import TodoEntity from '../data/entities'
import { BadRequest, ok } from '../HTTPError'

const deleteItem = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    try {
        const {name, dateCreated} = await parse(event)
        ok(name && dateCreated, BadRequest('Missing `isCompleted` or `dateCreated`'))

        console.debug('formData', {name, dateCreated})

        const key = {
            name,
            dateCreated: parseInt(dateCreated),
        }

        const resp = await TodoEntity
            .build(DeleteItemCommand)
            .key(key)
            .options({
                returnValues: 'ALL_OLD',
            })
            .send()

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(resp),
        }
    } catch (err) {
        const { message } = err as Error
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ message }),
        }
    }
}

export default deleteItem

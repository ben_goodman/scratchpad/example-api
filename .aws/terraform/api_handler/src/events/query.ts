import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'
import { ScanCommand, type ScanOptions } from 'dynamodb-toolbox/table/actions/scan'
import type { Condition } from 'dynamodb-toolbox/entity/actions/parseCondition'
import TodoTable from '../data/table'
import { BadRequest, ok } from '../HTTPError'
import TodoEntity from '../data/entities'

const queryNameCondition = (query: string): Condition<typeof TodoEntity> => ({
    attr: 'name',
    contains: query,
})

const queryItem = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    try {
        const { query } = event.pathParameters || {}

        ok(query, BadRequest('Missing `query`'))

        const scanOptions: ScanOptions<typeof TodoTable, [typeof TodoEntity]> = {
            filters: {
                TODO: queryNameCondition(query!),
            },
        }

        console.log('scanOptions', scanOptions)

        const resp = await TodoTable
            .build(ScanCommand)
            .entities(TodoEntity)
            .options(scanOptions)
            .send()

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(resp),
        }
    } catch (err) {
        const { message } = err as Error
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ message }),
        }
    }
}

export default queryItem

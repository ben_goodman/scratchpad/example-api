terraform {
    backend "s3" {
        bucket = "tf-states-us-east-1-bgoodman"
        key    = "example-fullstack-v2/terraform.tfstate"
        region = "us-east-1"
    }
}
output "gateway_base_url" {
    value = aws_apigatewayv2_stage.default.invoke_url
}

output "bucket_name" {
    value = module.website.bucket_name
}

output "cloudfront_id" {
    value = module.website.cloudfront_id
}

output "cloudfront_default_domain" {
    value = module.website.cloudfront_default_domain
}

output "user_pool_client_id" {
    value = aws_cognito_user_pool_client.default.id
}

output "aws_region" {
    value = data.aws_region.current.name
}
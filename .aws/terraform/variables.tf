variable "dynamo_table_name" {
    type = string
}

variable "project_name" {
    type = string
}

variable "resource_namespace" {
    type = string
}

variable "login_redirect_uri" {
    type = string
    default = "http://localhost:1234"
}